"use strict"

const { test, trait } = use("Test/Suite")("Tasks")
const Factory = use("Factory")

trait("DatabaseTransactions")
trait("Test/Browser")

test("Viewing tasks", async ({ browser }) => {
  const task = await Factory.model("App/Models/Task").create()
  const page = await browser.visit("/")

  await page.assertHas(task.title)
})


test("Adding a task", async ({ browser }) => {
  const page = await browser.visit("/")
  await page.type('[name="title"]', 'New Task')
  await page.submitForm('form[name=add]')
  await page.assertHas('New Task')
})

test("Deleting a task", async ({ browser }) => {
  const task = await Factory.model("App/Models/Task").create()
  const page = await browser.visit("/")
  await page.click('button.is-danger')
  await page.assertHas(task.title) == false
})
